﻿using System;
using System.IO;
using System.Linq;

namespace DateFolders
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length <= 0)
            {
                return;
            }

            foreach (var _directoryInfo in args.Select(arg => new DirectoryInfo(arg)))
            {
                _directoryInfo.GetDirectories()
                    .Where(IsEmpty)
                    .ToList()
                    .ForEach(Delete);

                var _dateDirectory = CreateSubdirectory(_directoryInfo, DateTime.Now.Date.ToString("yyyy-MM-dd"));
                var _downloadsDirectory = CreateSubdirectory(_dateDirectory, "Downloads");

                var _junctionPoint = _directoryInfo.GetDirectories("Downloads").SingleOrDefault();
                if (_junctionPoint != null && JunctionPoint.Exists(_junctionPoint.FullName))
                {
                    JunctionPoint.Delete(_junctionPoint.FullName);
                }

                if (!_directoryInfo.GetDirectories("Downloads").Any())
                {
                    JunctionPoint.Create(Path.Combine(_directoryInfo.FullName, "Downloads"), _downloadsDirectory.FullName, false);
                }
            }
        }

        private static DirectoryInfo CreateSubdirectory(DirectoryInfo directoryInfo, string directoryName)
        {
            return !directoryInfo.GetDirectories(directoryName).Any()
                       ? directoryInfo.CreateSubdirectory(directoryName)
                       : directoryInfo.GetDirectories(directoryName).Single();
        }

        private static bool IsEmpty(DirectoryInfo info)
        {
            return info.GetFiles().Length == 0 && info.GetDirectories().All(IsEmpty);
        }

        private static void Delete(DirectoryInfo info)
        {
            try
            {
                info.Delete(true);
            }
            catch
            {
            }
        }
    }
}
